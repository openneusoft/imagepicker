/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zzti.imagepickersample.slice;

import com.zzti.fengyongge.imagepicker.ImagePickerInstance;
import com.zzti.fengyongge.imagepicker.model.PhotoModel;
import com.zzti.fengyongge.imagepicker.util.CommonUtils;
import com.zzti.fengyongge.imagepicker.util.Glide;
import com.zzti.imagepickersample.ResourceTable;
import com.zzti.imagepickersample.model.UploadGoodsBean;
import com.zzti.imagepickersample.utils.SizeUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;

public class AddPicAbilitySlice extends AbilitySlice {

    /** 展示的图片集合 */
    private ArrayList<UploadGoodsBean> imageList = new ArrayList<UploadGoodsBean>();
    private int screen_widthOffset;
    private ListContainer myGridView;
    private GridImgAdapter gridImgsAdapter;
    private int requestCodeNum = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_add_pic);

        int width = CommonUtils.getWidthPixels(this);
        screen_widthOffset = (width - (3 * SizeUtils.dp2px(this,2))) / 3;
        myGridView = (ListContainer) findComponentById(ResourceTable.Id_my_goods_GV);
        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(3);
        myGridView.setLayoutManager(tableLayoutManager);
        gridImgsAdapter = new GridImgAdapter();
        myGridView.setItemProvider(gridImgsAdapter);
        imageList.add(null);
        gridImgsAdapter.notifyDataChanged();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    class GridImgAdapter extends BaseItemProvider {

        @Override
        public int getCount() {
            return imageList.size();
        }

        @Override
        public Object getItem(int position) {
            return imageList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Component getComponent(final int position, Component convertComponent, ComponentContainer parent) {
            convertComponent = LayoutScatter.getInstance(AddPicAbilitySlice.this).parse(
                    ResourceTable.Layout_ability_addstory_img_item, null, false);
            ComponentHolder holder;
            if (convertComponent != null) {
                holder = new ComponentHolder();
                convertComponent.setTag(holder);
            } else {
                holder = (ComponentHolder) convertComponent.getTag();
            }

            holder.add_IB = (Image) convertComponent.findComponentById(ResourceTable.Id_add_IB);
            holder.delete_IV = (Image) convertComponent.findComponentById(ResourceTable.Id_delete_IV);

            ComponentContainer.LayoutConfig config =
                    new ComponentContainer.LayoutConfig(screen_widthOffset, screen_widthOffset);
            convertComponent.setLayoutConfig(config);
            if (imageList.get(position) == null) {
                holder.delete_IV.setVisibility(Component.HIDE);
                holder.add_IB.setPixelMap(ResourceTable.Media_iv_add_the_pic);
                holder.add_IB.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        int limit = 9 - (imageList.size() - 1);
                        ImagePickerInstance.getInstance().photoSelect(
                                AddPicAbilitySlice.this, limit,true, requestCodeNum);
                    }
                });
            } else {
                Glide.with(AddPicAbilitySlice.this).load(imageList.get(position).getUrl()).into(holder.add_IB);
                holder.delete_IV.setClickedListener(new Component.ClickedListener() {
                    private boolean is_addNull;

                    @Override
                    public void onClick(Component component) {
                        is_addNull = true;
                        imageList.remove(position);
                        for (int i = 0; i < imageList.size(); i++) {
                            if (imageList.get(i) == null) {
                                is_addNull = false;
                                continue;
                            }
                        }
                        if (is_addNull) {
                            imageList.add(null);
                        }
                        gridImgsAdapter.notifyDataChanged();
                    }
                });

                holder.add_IB.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                       ArrayList<PhotoModel> tempList = new ArrayList<PhotoModel>();
                        for (int i = 0; i < imageList.size(); i++) {
                            if (imageList.get(i)!=null) {
                                PhotoModel photoModel = new PhotoModel();
                                photoModel.setOriginalPath(imageList.get(i).getUrl());
                                tempList.add(photoModel);
                            }
                        }
                        ImagePickerInstance.getInstance().photoPreview(
                                AddPicAbilitySlice.this, tempList, position,false);
                    }
                });
            }
            return convertComponent;
        }

        class ComponentHolder {
            Image add_IB;
            Image delete_IV;
        }
    }

    @Override
    public void onAbilityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 0:
                if (data != null) {
                    List<String> paths = data.getStringArrayListParam("photos");
                    if (imageList.size() > 0) {
                        imageList.remove(imageList.size() - 1);
                    }

                    for (int i = 0; i < paths.size(); i++) {
                        imageList.add(new UploadGoodsBean(paths.get(i), false));
                        //上传参数
                    }
                    if (imageList.size() < 9) {
                        imageList.add(null);
                    }
                    gridImgsAdapter.notifyDataChanged();
                }
                break;
            default:
                break;
        }
        super.onAbilityResult(requestCode, resultCode, data);
    }

}
