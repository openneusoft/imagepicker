/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zzti.imagepickersample.slice;

import com.zzti.fengyongge.imagepicker.ImagePickerInstance;
import com.zzti.fengyongge.imagepicker.model.PhotoModel;
import com.zzti.imagepickersample.ResourceTable;
import com.zzti.imagepickersample.model.TestBean;
import com.zzti.imagepickersample.view.CustomImage;
import com.zzti.imagepickersample.view.NineGridlayout;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;

public class PreViewAbilitySlice extends AbilitySlice {

    private ListContainer recyclerView;
    private MyAdapter myAdapter;
    List<TestBean> list = new ArrayList<>();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_pre_view);

        recyclerView = (ListContainer) findComponentById(ResourceTable.Id_recycleView);
        recyclerView.setLayoutManager(new DirectionalLayoutManager());
        myAdapter = new MyAdapter();
        recyclerView.setItemProvider(myAdapter);
        loadMore();
        myAdapter.notifyDataChanged();
    }

    public void loadMore() {
        TestBean testBean = new TestBean();
        List<String> list1 = new ArrayList<>();
        list1.add("http://gank.io/images/f0c192e3e335400db8a709a07a891b2e");
        testBean.setPic(list1);
        list.add(testBean);

        TestBean testBean1 = new TestBean();
        List<String> list2 = new ArrayList<>();
        list2.add("http://gank.io/images/f4f6d68bf30147e1bdd4ddbc6ad7c2a2");
        list2.add("http://gank.io/images/dc75cbde1d98448183e2f9514b4d1320");
        list2.add("http://gank.io/images/bdb35e4b3c0045c799cc7a494a3db3e0");
        testBean1.setPic(list2);
        list.add(testBean1);
    }

    @Override
    public void onActive() {
        super.onActive();
//        list.clear();
//        myAdapter.notifyDataChanged();
//        loadMore();
//        myAdapter.notifyDataChanged();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    class MyAdapter extends BaseItemProvider {

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
            component = LayoutScatter.getInstance(PreViewAbilitySlice.this).parse(
                    ResourceTable.Layout_ability_pre_view_item, null, false);
            RecyclerViewViewHolder holder = new RecyclerViewViewHolder(component);
            if (list.get(position).getPic().size() == 1) {
                holder.iv_oneimage.setVisibility(Component.VISIBLE);
                holder.iv_ngrid_layout.setVisibility(Component.HIDE);
                holder.iv_oneimage.setImageUrl(list.get(position).getPic().get(0));

                holder.iv_oneimage.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        ArrayList<PhotoModel> tempList = new ArrayList<PhotoModel>();
                        for (int i = 0; i < list.get(position).getPic().size(); i++) {
                            PhotoModel photoModel = new PhotoModel();
                            photoModel.setOriginalPath(list.get(position).getPic().get(i));
                            tempList.add(photoModel);
                        }
                        ImagePickerInstance.getInstance().photoPreview(PreViewAbilitySlice.this, tempList, position, false);
                    }
                });
            } else {
                holder.iv_oneimage.setVisibility(Component.HIDE);
                holder.iv_ngrid_layout.setVisibility(Component.VISIBLE);
                final ArrayList<String> list1 = (ArrayList<String>) list.get(position).getPic();

                ArrayList<PhotoModel> tempList = new ArrayList<PhotoModel>();

                for (int i = 0; i < list1.size(); i++) {
                    PhotoModel photoModel = new PhotoModel();
                    photoModel.setOriginalPath(list.get(position).getPic().get(i));
                    tempList.add(photoModel);
                }

                holder.iv_ngrid_layout.setImagesData(tempList, PreViewAbilitySlice.this);
            }
            return component;
        }

        class RecyclerViewViewHolder {
            CustomImage iv_oneimage;
            NineGridlayout iv_ngrid_layout;

            public RecyclerViewViewHolder(Component component) {
                iv_oneimage = (CustomImage) component.findComponentById(ResourceTable.Id_iv_oneimage);
                iv_ngrid_layout = (NineGridlayout) component.findComponentById(ResourceTable.Id_iv_ngrid_layout);
            }
        }
    }

}
