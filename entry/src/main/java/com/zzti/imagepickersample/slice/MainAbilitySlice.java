/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zzti.imagepickersample.slice;

import com.zzti.imagepickersample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;

public class MainAbilitySlice extends AbilitySlice {

    private Button btAddPic, btPreView, btSave;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        initView();
        initOnclick();
    }

    void initView() {
        btAddPic = (Button) findComponentById(ResourceTable.Id_btAddPic);
        btPreView = (Button) findComponentById(ResourceTable.Id_btPreView);
        btSave = (Button) findComponentById(ResourceTable.Id_btSave);
    }

    void initOnclick() {
        if (btAddPic != null) {
            btAddPic.setClickedListener(listener -> present(new AddPicAbilitySlice(), new Intent()));
        }

        if (btPreView != null) {
            btPreView.setClickedListener(listener -> present(new PreViewAbilitySlice(), new Intent()));
        }

        if (btSave != null) {
            btSave.setClickedListener(listener -> present(new SaveAbilitySlice(), new Intent()));
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
