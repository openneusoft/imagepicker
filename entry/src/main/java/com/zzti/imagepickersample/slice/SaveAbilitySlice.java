package com.zzti.imagepickersample.slice;

import com.zzti.fengyongge.imagepicker.ImagePickerInstance;
import com.zzti.fengyongge.imagepicker.model.PhotoModel;
import com.zzti.fengyongge.imagepicker.util.Glide;
import com.zzti.imagepickersample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ohos.agp.components.Component;
import ohos.agp.components.Image;

import java.util.ArrayList;

public class SaveAbilitySlice extends AbilitySlice {

    String url = "http://gank.io/images/f0c192e3e335400db8a709a07a891b2e";
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_save);

        Image ivDownload = (Image) findComponentById(ResourceTable.Id_ivDownload);

        Glide.with(this).load(url).into(ivDownload);
        findComponentById(ResourceTable.Id_tvSave).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ArrayList<PhotoModel> tempList = new ArrayList<PhotoModel>();
                PhotoModel photoModel = new PhotoModel();
                photoModel.setOriginalPath(url);
                tempList.add(photoModel);
                ImagePickerInstance.getInstance().photoPreview(SaveAbilitySlice.this, tempList, 0, true);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

}
