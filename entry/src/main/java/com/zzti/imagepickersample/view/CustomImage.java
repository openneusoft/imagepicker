package com.zzti.imagepickersample.view;

import com.zzti.fengyongge.imagepicker.util.Glide;
import com.zzti.fengyongge.imagepicker.util.TextUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.app.Context;

public class CustomImage extends Image implements Component.BindStateChangedListener {
    private String url;
    private boolean isAttachedToWindow;

    public CustomImage(Context context) {
        super(context);
        setBindStateChangedListener(this);
    }

    public CustomImage(Context context, AttrSet attrSet) {
        super(context, attrSet);
        setBindStateChangedListener(this);
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        isAttachedToWindow = true;
        setImageUrl(url);
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        isAttachedToWindow = false;
        setPixelMap(null);
    }

    public void setImageUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            this.url = url;
            if (isAttachedToWindow) {
                Glide.with(getContext()).load(url).into(this);
            }
        }
    }
}
