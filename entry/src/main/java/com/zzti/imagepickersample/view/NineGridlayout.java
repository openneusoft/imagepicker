package com.zzti.imagepickersample.view;

import com.zzti.fengyongge.imagepicker.ImagePickerInstance;
import com.zzti.fengyongge.imagepicker.model.PhotoModel;
import com.zzti.imagepickersample.utils.ScreenTools;
import com.zzti.imagepickersample.utils.SizeUtils;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class NineGridlayout extends ComponentContainer {

    /**
     * 图片之间的间隔
     */
    private int gap = 10;
    private int columns;
    private int rows;
    private ArrayList<PhotoModel> listData;
    private int totalWidth;
    private Context context;
    private CustomImage childrenView;

    public NineGridlayout(Context context) {
        super(context);
    }

    public NineGridlayout(Context context, AttrSet attrs) {
        super(context, attrs);
        ScreenTools screenTools= ScreenTools.instance(getContext());
        totalWidth=screenTools.getScreenWidth()- SizeUtils.dp2px(context,80);
    }

    private void layoutChildrenView(){
        int childrenCount = listData.size();

        int singleWidth = (totalWidth - gap * (3 - 1)) / 3;
        int singleHeight = singleWidth;

        //根据子view数量确定高度
        LayoutConfig params = getLayoutConfig();
        params.height = singleHeight * rows + gap * (rows - 1);
        setLayoutConfig(params);

        for (int i = 0; i < childrenCount; i++) {
            childrenView = (CustomImage) getComponentAt(i);
            childrenView.setImageUrl((listData.get(i)).getOriginalPath());

            int[] position = findPosition(i);
            int left = (singleWidth + gap) * position[1];
            int top = (singleHeight + gap) * position[0];
            int right = left + singleWidth;
            int bottom = top + singleHeight;

            childrenView.setComponentPosition(left, top, right, bottom);
            childrenView.setWidth(singleWidth);
            childrenView.setHeight(singleHeight);
            childrenView.setTag(i);
            childrenView.setClickedListener(new ClickedListener() {
                @Override
                public void onClick(Component component) {
                    ArrayList<PhotoModel> tempList = new ArrayList<PhotoModel>(listData);
                    //展示多张图片
                    ImagePickerInstance.getInstance().photoPreview(
                            context, tempList, Integer.parseInt(component.getTag().toString()),false);
                }
            });
            childrenView.setLongClickedListener(new LongClickedListener() {
                @Override
                public void onLongClicked(Component component) {
                }
            });
        }
    }

    public void setImagesData(ArrayList<PhotoModel> lists, Context context) {
        this.context = context;
        if (lists == null || lists.isEmpty()) {
            return;
        }
        //初始化布局
        generateChildrenLayout(lists.size());
        //这里做一个重用view的处理
        if (listData == null) {
            int i = 0;
            while (i < lists.size()) {
                addComponent(generateImageView(), getLayoutConfig());
                i++;
            }
        } else {
            int oldViewCount = listData.size();
            int newViewCount = lists.size();
            if (oldViewCount > newViewCount) {
                removeComponents(newViewCount - 1, oldViewCount - newViewCount);
            } else if (oldViewCount < newViewCount) {
                for (int i = 0; i < newViewCount - oldViewCount; i++) {
                    addComponent(generateImageView(), getLayoutConfig());
                }
            }
        }
        listData = lists;
        layoutChildrenView();
    }


    /**
     * 根据图片个数确定行列数量
     * 对应关系如下
     * num	row	column
     * 1	   1	1
     * 2	   1	2
     * 3	   1	3
     * 4	   2	2
     * 5	   2	3
     * 6	   2	3
     * 7	   3	3
     * 8	   3	3
     * 9	   3	3
     *
     * @param length
     */
    private void generateChildrenLayout(int length) {
        if (length <= 3) {
            rows = 1;
            columns = length;
        } else if (length <= 6) {
            rows = 2;
            columns = 3;
            if (length == 4) {
                columns = 2;
            }
        } else {
            rows = 3;
            columns = 3;
        }
    }

    private CustomImage generateImageView() {
        CustomImage iv = new CustomImage(getContext());
        iv.setScaleMode(Image.ScaleMode.CLIP_CENTER);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#f5f5f5")));
        iv.setBackground(element);
        return iv;
    }


    private int[] findPosition(int childNum) {
        int[] position = new int[2];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if ((i * columns + j) == childNum) {
                    position[0] = i;//行
                    position[1] = j;//列
                    break;
                }
            }
        }
        return position;
    }

    public int getGap() {
        return gap;
    }

    public void setGap(int gap) {
        this.gap = gap;
    }
}
