package com.zzti.imagepickersample;

import com.zzti.fengyongge.imagepicker.ImageloderListener.ImageDownloadListener;
import com.zzti.fengyongge.imagepicker.ImageloderListener.ImageloderImpl;
import com.zzti.fengyongge.imagepicker.ImageloderListener.ImageloderListener;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ImageloderImplTest {

    @Test
    public void asyncDownloadImage() throws IOException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ImageDownloadListener listener = new ImageDownloadListener() {
            @Override
            public void onDownloadSuccess() {
            }

            @Override
            public void onDownloadFail() {
            }
        };
        String url = "http://gank.io/images/f0c192e3e335400db8a709a07a891b2e";
        String savePath = "imagepicker";
        String fileName = "IMG_123.jpg";
        ImageloderListener imageloderListener = new ImageloderImpl();
        imageloderListener.asyncDownloadImage(context, url, savePath, fileName, listener);
    }
}