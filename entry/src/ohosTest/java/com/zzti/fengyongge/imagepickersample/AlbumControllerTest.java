package com.zzti.imagepickersample;

import com.zzti.fengyongge.imagepicker.control.AlbumController;
import com.zzti.fengyongge.imagepicker.model.AlbumModel;
import com.zzti.fengyongge.imagepicker.model.PhotoModel;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class AlbumControllerTest {

    private DataAbilityHelper helper;
    
    @Test
    public void getCurrent() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        AlbumController albumController = new AlbumController(context);
        List<PhotoModel> photoList =  albumController.getCurrent();
    }

    @Test
    public void getAlbums() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        AlbumController albumController = new AlbumController(context);
        List<AlbumModel> albumsList =  albumController.getAlbums();
    }

    @Test
    public void getAlbum() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        AlbumController albumController = new AlbumController(context);
        List<PhotoModel> albumList =  albumController.getAlbum("Camera");
    }
}