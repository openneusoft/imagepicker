package com.zzti.imagepickersample.utils;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class ScreenToolsTest {
    private static ScreenTools mScreenTools;
    @Test
    public void instance() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    }

    @Test
    public void getDensity() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        float densityPixels = ScreenTools.instance(context).getDensity(context);
        assertEquals(3.0, densityPixels, 0.0);
    }

    @Test
    public void getScreenWidth() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int densityPixels = ScreenTools.instance(context).getScreenWidth();
        assertEquals(2086, densityPixels);
    }
}