package com.zzti.imagepickersample.utils;

import com.zzti.fengyongge.imagepicker.util.CommonUtils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Test;

import static org.junit.Assert.*;

public class SizeUtilsTest {
    @Test
    public void dp2px() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int densityPixels = SizeUtils.dp2px(context,0.5f);
        assertEquals(2, densityPixels);
    }

    @Test
    public void px2dp() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int densityPixels = SizeUtils.px2dp(context,30f);
        assertEquals(10, densityPixels);

    }
}