package com.zzti.imagepickersample;

import com.zzti.fengyongge.imagepicker.util.CommonUtils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CommonUtilsTest {

    @Test
    public void getWidthPixels() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int densityPixels = CommonUtils.getWidthPixels(context);
        assertEquals(2086, densityPixels);
    }
}