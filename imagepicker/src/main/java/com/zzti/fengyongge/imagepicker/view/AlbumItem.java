package com.zzti.fengyongge.imagepicker.view;

import com.zzti.fengyongge.imagepicker.ImageloderListener.Imageloder;
import com.zzti.fengyongge.imagepicker.ResourceTable;
import com.zzti.fengyongge.imagepicker.model.AlbumModel;
import ohos.agp.components.*;
import ohos.app.Context;

/**
 * Created by fengyongge on 2016/5/24
 */
public final class AlbumItem extends DirectionalLayout {

    private Image ivAlbum, ivIndex;
    private Text tvName, tvCount;
    private Context context;

    public AlbumItem(Context context) {
        this(context, null);
    }

    public AlbumItem(Context context, AttrSet attrs) {
        super(context, attrs);
        this.context = context;
        LayoutScatter.getInstance(context).parse(
                ResourceTable.Layout_layout_album, this, true);
        ivAlbum = (Image) findComponentById(ResourceTable.Id_iv_album_la);
        ivIndex = (Image) findComponentById(ResourceTable.Id_iv_index_la);
        tvName = (Text) findComponentById(ResourceTable.Id_tv_name_la);
        tvCount = (Text) findComponentById(ResourceTable.Id_tv_count_la);
    }

    public AlbumItem(Context context, AttrSet attrs, int defStyle) {
        this(context, attrs);
    }

    /** 设置相册封面 */
    public void setAlbumImage(String path) {
        Imageloder.getInstance().loadImageUrl(context, path, ivAlbum);
    }

    /** 初始化 */
    public void update(AlbumModel album) {
        setAlbumImage(album.getRecent());
        setName(album.getName());
        setCount(album.getCount());
        isCheck(album.isCheck());
    }

    public void setName(String title) {
        tvName.setText(title);
    }

    public void setCount(int count) {
        tvCount.setHint(count + "张");
    }

    public void isCheck(boolean isCheck) {
        if (isCheck){
            ivIndex.setVisibility(Component.VISIBLE);
        } else {
            ivIndex.setVisibility(Component.HIDE);
        }
    }
}
