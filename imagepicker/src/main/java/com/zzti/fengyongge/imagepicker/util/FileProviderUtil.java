package com.zzti.fengyongge.imagepicker.util;

import ohos.utils.net.Uri;

import java.io.File;

/**
 * Created by fengyongge on 2018/8/21
 */
public final class FileProviderUtil {
    private FileProviderUtil() {}
    public static Uri getFileUri(File file) {
        return Uri.getUriFromFile(file);
    }
}
