/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zzti.fengyongge.imagepicker;

import com.zzti.fengyongge.imagepicker.control.PhotoSelectorDomain;
import com.zzti.fengyongge.imagepicker.model.PhotoModel;
import ohos.aafwk.content.Intent;

import java.util.List;

public final class PhotoPreviewAbility extends BasePhotoPreviewAbility implements
        PhotoSelectorAbility.OnLocalRecentListener {
    private PhotoSelectorDomain photoSelectorDomain;
    private boolean isSave;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        photoSelectorDomain = new PhotoSelectorDomain(getApplicationContext());
        init(intent);
    }

    protected void init(Intent intent) {
        isSave = intent.getBooleanParam(ImagePickerInstance.IS_SAVE, false);
        if (intent.hasParameter(ImagePickerInstance.PHOTOS)) {
            this.photos = intent.getSequenceableArrayListParam(ImagePickerInstance.PHOTOS);
            this.current = intent.getIntParam(ImagePickerInstance.POSITION, 0);
            if (isSave) {
                bindData(true);
            } else {
                bindData(false);
            }
            updatePercent(current, photos.size());
        } else if (intent.hasParameter("album")) {
            String albumName = intent.getStringParam("album");
            this.current = intent.getIntParam("position", 0);
            if (albumName != null && albumName.equals(PhotoSelectorAbility.RECCENT_PHOTO)) {
                photoSelectorDomain.getReccent(this);
            } else {
                photoSelectorDomain.getAlbum(albumName, this);
            }
        }
    }

    /**
     * 图库图片预览
     * @param photos
     */
    @Override
    public void onPhotoLoaded(List<PhotoModel> photos) {
        this.photos = photos;
        //绑定数据，更新展示张数，不可少
        bindData(false);
        updatePercent(current,photos.size());

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
