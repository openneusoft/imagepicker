package com.zzti.fengyongge.imagepicker.util;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;
import ohos.app.Environment;

import java.io.File;
import java.io.IOException;

public final class FileUtils {

    private FileUtils() {}

    /**
     * 获取拍照图片图库地址
     */
    public static File getCreateFilePath(Context context){
        File takeImageFile = null;
        try {
            takeImageFile = FileUtils.createFile(
                    context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getCanonicalFile(),
                    "IMG_", ".jpg").getCanonicalFile();
        } catch (IOException e) {
            LogUtils.error(context.getClass().getSimpleName(), e.getMessage());
        }

        return takeImageFile;
    }

    /**
     * 根据系统时间、前缀、后缀产生一个文件
     */
    public static File createFile(File folder, String prefix, String suffix) {
        if (!folder.exists() || !folder.isDirectory()) {
            folder.mkdirs();
        }
        String filename = prefix + System.currentTimeMillis() + suffix;
        return new File(folder, filename);
    }

    /**
     * 刷新图库
     */
    public static void updateGallery(Context context, File file) {
        Intent mediaScanIntent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withAction("android.intent.action.MEDIA_SCANNER_SCAN_FILE")
//                .withUri(Uri.getUriFromFile(file))
                .build();
        mediaScanIntent.setOperation(operation);
        context.startAbility(mediaScanIntent, 0);
    }
}
