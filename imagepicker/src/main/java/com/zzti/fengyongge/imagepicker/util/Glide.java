/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zzti.fengyongge.imagepicker.util;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.agp.components.Image;
import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;
import okhttp3.*;

import java.io.*;

public final class Glide {
    private Context context;
    private String url;
    private Image image;

    public Glide(Context context) {
        this.context = context;
    }

    public static Glide with(Context context) {
        return new Glide(context);
    }

    public Glide load(String url) {
        this.url = url;
        return this;
    }

    public void into(Image image) {
        this.image = image;
        if (url.startsWith("http") || url.startsWith("https")) {
            loadImageFromNetWork();
        } else if (url.startsWith("dataability")) {
            loadImageFromDataAbility();
        } else {
            loadImageFromLocalPath();
        }
    }

    private void loadImageFromNetWork() {
        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder().get().url(url).build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            public void onFailure(Call call, IOException e) {
                LogUtils.error(context.getClass().getSimpleName(), "Fail:" + e.getMessage());
            }

            public void onResponse(Call call, Response response) throws IOException {
                ImageSource imageSource = ImageSource.create(response.body().byteStream(), null);
                //根据图片源创建位图
                PixelMap pixelMap = imageSource.createPixelmap(null);
                //需要异步渲染UI
                context.getUITaskDispatcher().asyncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        image.setPixelMap(pixelMap);
                        pixelMap.release();
                    }
                });
            }
        });
    }

    private void loadImageFromLocalPath() {
        try {
            File file = new File(url);
            ImageSource imageSource = ImageSource.create(new FileInputStream(file),null);
            PixelMap pixelMap = imageSource.createThumbnailPixelmap(null,true);
            context.getUITaskDispatcher().asyncDispatch(new Runnable() {
                @Override
                public void run() {
                    image.setPixelMap(pixelMap);
                    pixelMap.release();
                }
            });
        } catch(FileNotFoundException e) {
            LogUtils.error(image.getContext().getClass().getSimpleName(), e.getMessage());
        }

    }

    private void loadImageFromDataAbility() {
        try {
            DataAbilityHelper helper = DataAbilityHelper.creator(context);
            FileDescriptor fileDesc = helper.openFile(Uri.parse(url),"r");
            ImageSource imageSource = ImageSource.create(fileDesc,null);
            PixelMap pixelMap = imageSource.createThumbnailPixelmap(null,true);
            context.getUITaskDispatcher().asyncDispatch(new Runnable() {
                @Override
                public void run() {
                    image.setPixelMap(pixelMap);
                    pixelMap.release();
                }
            });
        } catch (FileNotFoundException e) {
            LogUtils.error(image.getContext().getClass().getSimpleName(), e.getMessage());
        } catch (DataAbilityRemoteException e) {
            LogUtils.error(image.getContext().getClass().getSimpleName(), e.getMessage());
        }
    }
}
