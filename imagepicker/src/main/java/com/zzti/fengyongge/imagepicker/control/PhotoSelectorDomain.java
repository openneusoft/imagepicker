package com.zzti.fengyongge.imagepicker.control;

import com.zzti.fengyongge.imagepicker.PhotoSelectorAbility;
import com.zzti.fengyongge.imagepicker.model.AlbumModel;
import com.zzti.fengyongge.imagepicker.model.PhotoModel;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import java.util.List;

/**
 * Created by fengyongge on 2016/5/24
 */
public final class PhotoSelectorDomain {

    private AlbumController albumController;

    public PhotoSelectorDomain(Context context) {
        albumController = new AlbumController(context);
    }

    @SuppressWarnings("unchecked")
    public void getReccent(final PhotoSelectorAbility.OnLocalRecentListener listener) {
        final EventHandler handler = new EventHandler(EventRunner.current()) {
            @Override
            public void processEvent (InnerEvent event) {
                List<PhotoModel> tempPhotos = (List<PhotoModel>) event.object;
                listener.onPhotoLoaded(tempPhotos);
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<PhotoModel> photos = albumController.getCurrent();
                InnerEvent event = InnerEvent.get(1, photos);
                handler.sendEvent(event);
            }
        }).start();
    }

    /** 获取相册列表 */
    @SuppressWarnings("unchecked")
    public void updateAlbum(final PhotoSelectorAbility.OnLocalAlbumListener listener) {
        final EventHandler handler = new EventHandler(EventRunner.current()) {
            @Override
            public void processEvent(InnerEvent event) {
                List<AlbumModel> albums = (List<AlbumModel>) event.object;
                int index = 0;
                for (int i = 0; i < albums.size(); i++) {
                    if (albums.get(i).getName().trim().equals("images")) {
                        index = i;
                    }
                }
                if(albums.size()>0){
                    albums.remove(index);
                }
                listener.onAlbumLoaded(albums);
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<AlbumModel> albums = albumController.getAlbums();
                InnerEvent event = InnerEvent.get(1, albums);
                handler.sendEvent(event);
            }
        }).start();
    }

    /** 获取单个相册下的所有照片信息 */
    @SuppressWarnings("unchecked")
    public void getAlbum(final String name, final PhotoSelectorAbility.OnLocalRecentListener listener) {
        final EventHandler handler = new EventHandler(EventRunner.current()) {
            @Override
            public void processEvent(InnerEvent event) {
                listener.onPhotoLoaded((List<PhotoModel>) event.object);
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<PhotoModel> photos = albumController.getAlbum(name);
                InnerEvent event = InnerEvent.get(1, photos);
                handler.sendEvent(event);
            }
        }).start();
    }
}
