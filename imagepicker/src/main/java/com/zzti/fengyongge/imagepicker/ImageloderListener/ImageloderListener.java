package com.zzti.fengyongge.imagepicker.ImageloderListener;

import ohos.agp.components.Image;
import ohos.app.Context;

/**
 * 图片加载接口
 * @author fengyongge
 * GitHub:https://github.com/fengyongge/imagepicker
 */
public interface ImageloderListener {

    void loadImageUrl(Context context, String url, Image image);

    void asyncDownloadImage(Context context, String url, String savePath, String saveFileName, ImageDownloadListener listener);

}
