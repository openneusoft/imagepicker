package com.zzti.fengyongge.imagepicker.model;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

/**
 * Created by fengyongge on 2016/5/24
 */
public final class PhotoModel implements Sequenceable {

    /**
     * 网络图片url：http://gank.io/images/f0c192e3e335400db8a709a07a891b2e 或者 https://gank.io/images/f0c192e3e335400db8a709a07a891b2e
     * 本地图片uri：dataability:///storage/emulated/0/camera/1
     */
    private String originalPath;

    /**
     * 是否选中
     */
    private boolean isChecked = false;

    public PhotoModel() {
    }

    public String getOriginalPath() {
        return originalPath;
    }

    public void setOriginalPath(String originalPath) {
        this.originalPath = originalPath;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    @Override
    public boolean marshalling(Parcel out) {
        return out.writeString(originalPath) && out.writeBoolean(isChecked);
    }

    @Override
    public boolean unmarshalling(Parcel in) {
        this.originalPath = in.readString();
        this.isChecked = in.readBoolean();
        return true;
    }

    public static final Sequenceable.Producer PRODUCER  = new Sequenceable.Producer() {
        @Override
        public PhotoModel createFromParcel(Parcel in) {
            PhotoModel instance = new PhotoModel();
            instance.unmarshalling(in);
            return instance;
        }
    };
}
