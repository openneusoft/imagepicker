package com.zzti.fengyongge.imagepicker.adapter;

import com.zzti.fengyongge.imagepicker.ResourceTable;
import com.zzti.fengyongge.imagepicker.model.PhotoModel;
import com.zzti.fengyongge.imagepicker.util.LogUtils;
import com.zzti.fengyongge.imagepicker.view.SelectPhotoItem;
import ohos.agp.components.*;
import ohos.agp.components.Component.ClickedListener;
import ohos.agp.components.ComponentContainer.LayoutConfig;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by fengyongge on 2016/5/24
 */
public final class PhotoSelectorAdapter extends MBaseAdapter<PhotoModel> {
    private int itemWidth;
    private final int horizentalNum = 3;
    private SelectPhotoItem.PhotoItemCheckedListener listener;
    private LayoutConfig itemLayoutConfig;
    private SelectPhotoItem.ItemClickListener mCallback;
    private ClickedListener cameraListener;
    private int limit;
    private DirectionalLayout itemView;
    private PhotoSelectorAdapter(Context context, ArrayList<PhotoModel> models) {
        super(context, models);
    }

    public PhotoSelectorAdapter(Context context, ArrayList<PhotoModel> models, int screenWidth,
                                SelectPhotoItem.PhotoItemCheckedListener listener,
                                SelectPhotoItem.ItemClickListener mCallback, ClickedListener cameraListener, int limit) {
        this(context, models);
        setItemWidth(screenWidth);
        this.listener = listener;
        this.mCallback = mCallback;
        this.limit = limit;
        this.cameraListener = cameraListener;
    }

    /** 设置每一个Item的宽高 */
    public void setItemWidth(int screenWidth) {
        try {
            int horizontalSpace = (int) context.getResourceManager().getElement(
                    ResourceTable.Float_sticky_item_horizontalSpacing).getFloat();
            this.itemWidth = (screenWidth - (horizontalSpace * (horizentalNum - 1))) / horizentalNum;
            this.itemLayoutConfig = new LayoutConfig(itemWidth, itemWidth);
        } catch (IOException e) {
            LogUtils.error(context.getClass().getSimpleName(), e.getMessage());
        } catch (NotExistException e) {
            LogUtils.error(context.getClass().getSimpleName(), e.getMessage());
        } catch (WrongTypeException e) {
            LogUtils.error(context.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer parent) {
        SelectPhotoItem item = null;
        DependentLayout rlCamera = null;
        Component component = null;
        // 当时第一个时，显示按钮
        if (position == 0 && models.get(position).getOriginalPath() == null) {
            if (convertComponent == null || !(convertComponent instanceof DependentLayout)) {
                rlCamera = (DependentLayout) LayoutScatter.getInstance(context).parse(ResourceTable.Layout_view_camera,
                        null, false);
                rlCamera.setLayoutConfig(itemLayoutConfig);
                component = rlCamera;
            }
            component.setClickedListener(cameraListener);
        } else { // 显示图片
            if (convertComponent == null || !(convertComponent instanceof SelectPhotoItem)) {
                item = new SelectPhotoItem(context, listener, limit);
                item.setLayoutConfig(itemLayoutConfig);
                component = item;
            } else {
                item = (SelectPhotoItem) convertComponent;
            }
            item.setImageDrawable(models.get(position));
            item.setSelected(models.get(position).isChecked());
            item.setClickedListener(mCallback, position);
        }
        return component;
    }
}
