package com.zzti.fengyongge.imagepicker.util;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * 通用工具类
 * @author fengyongge
 */
public final class CommonUtils {

    private CommonUtils(){}

    /** 获取屏幕宽度 */
    public static int getWidthPixels(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        return display.getAttributes().width;
    }
}
