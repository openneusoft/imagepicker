package com.zzti.fengyongge.imagepicker.adapter;

import com.zzti.fengyongge.imagepicker.model.AlbumModel;
import com.zzti.fengyongge.imagepicker.view.AlbumItem;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by fengyongge on 2016/5/24
 */
public final class AlbumAdapter extends MBaseAdapter<AlbumModel> {
    public AlbumAdapter(Context context, ArrayList<AlbumModel> models) {
        super(context, models);
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer parent) {
        AlbumItem albumItem = null;
        Component componentItem = null;
        componentItem = component;
        if (component == null) {
            albumItem = new AlbumItem(context);
            componentItem = albumItem;
        } else{
            albumItem = (AlbumItem) component;
        }
        albumItem.update(models.get(position));
        return componentItem;
    }

}
