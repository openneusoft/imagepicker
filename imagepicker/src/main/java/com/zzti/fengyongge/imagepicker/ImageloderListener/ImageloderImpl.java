package com.zzti.fengyongge.imagepicker.ImageloderListener;

import com.zzti.fengyongge.imagepicker.util.FileUtilsExt;
import com.zzti.fengyongge.imagepicker.util.Glide;
import ohos.agp.components.Image;
import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import okhttp3.*;

import java.io.File;
import java.io.IOException;

/**
 * 图片加载实现方法
 * @author fengyongge
 * GitHub:https://github.com/fengyongge/imagepicker
 */
public final class ImageloderImpl implements ImageloderListener {


    @Override
    public void loadImageUrl(Context context, String url, Image image) {
        Glide.with(context).load(url).into(image);
    }

    @Override
    public void asyncDownloadImage(Context context, String url, String saveFileName, String savePath, ImageDownloadListener listener) {
        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder().get().url(url).build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            public void onFailure(Call call, IOException e) {
                listener.onDownloadFail();
            }
            public void onResponse(Call call, Response response) throws IOException {
                ImageSource imageSource = ImageSource.create(response.body().byteStream(), null);
                //根据图片源创建位图
                PixelMap pixelMap = imageSource.createPixelmap(null);
                File destFile = FileUtilsExt.savaFileUtils(context, saveFileName, savePath);
                FileUtilsExt.saveBitmap2File(context, pixelMap, destFile);
                listener.onDownloadSuccess();
            }
        });
    }
}
