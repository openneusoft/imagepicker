package com.zzti.fengyongge.imagepicker.util;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.app.Context;
import ohos.data.rdb.ValuesBucket;
import ohos.media.image.ImagePacker;
import ohos.media.image.ImageSource;
import ohos.media.photokit.metadata.AVStorage.Images.Media;
import ohos.utils.net.Uri;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * 通知系统相册更新
 * @author fengyongge
 * GitHub:https://github.com/fengyongge/imagepicker
 */
final class NotifyUtils {
    private static final int QUALITY = 100;
    private NotifyUtils() {}
    public static void refreshSystemPic(Context context, File destFile) {
        insertPic(context, destFile);
    }

    /**
     * 向系统相册插入图片
     */
    private static void insertPic(Context context, File insertFile) {
        try {
            ValuesBucket values = new ValuesBucket();
            values.putString(Media.DISPLAY_NAME, insertFile.getName());
            values.putString(Media.MIME_TYPE, "image/JPEG");
            values.putString("relative_path", "Pictures/");
            // 应用独占
            values.putInteger("is_pending", 1);
            DataAbilityHelper helper = DataAbilityHelper.creator(context);
            int id = helper.insert(Media.EXTERNAL_DATA_ABILITY_URI, values);
            Uri uri = Uri.appendEncodedPathToUri(Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(id));

            // 这里需要写的权限
            FileDescriptor fd = helper.openFile(uri, "w");
            ImagePacker imagePacker = ImagePacker.create();
            ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
            OutputStream outputStream = new FileOutputStream(fd);
            packingOptions.format = "image/jpeg";
            packingOptions.quality = QUALITY;

            boolean result = imagePacker.initializePacking(outputStream, packingOptions);
            if (result) {
                ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
                sourceOptions.formatHint = "image/jpeg";
                result = imagePacker.addImage(ImageSource.create(insertFile, sourceOptions));
                if (result) {
                    imagePacker.finalizePacking();
                }
            }
            outputStream.flush();
            outputStream.close();
            values.clear();
            // 接触独占
            values.putInteger("is_pending", 0);
            helper.update(uri, values, null);

        } catch (Exception e) {
            LogUtils.error(context.getClass().getSimpleName(), e.getMessage());
        }
    }

}
