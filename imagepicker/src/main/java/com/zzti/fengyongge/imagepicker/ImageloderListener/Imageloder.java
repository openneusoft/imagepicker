package com.zzti.fengyongge.imagepicker.ImageloderListener;

import ohos.agp.components.Image;
import ohos.app.Context;

/**
 * 图片加载对外API
 * @author fengyongge
 * GitHub:https://github.com/fengyongge/imagepicker
 */
public final class Imageloder {

    private ImageloderListener imageloderListener = new ImageloderImpl();

    private Imageloder(){

    }
    public static Imageloder getInstance(){
        return SingleInstance.INSTANCE;
    }


    public void loadImageUrl(Context context, String url, Image image){
        imageloderListener.loadImageUrl(context, url, image);
    }

    public void asyncDownloadImage(Context context, String url, String saveFileName, String savePath, ImageDownloadListener listener){
        imageloderListener.asyncDownloadImage(context, url, saveFileName, savePath, listener);
    }

    private static class SingleInstance{
        private static final Imageloder INSTANCE = new Imageloder();
    }

}
