/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zzti.fengyongge.imagepicker;

import com.zzti.fengyongge.imagepicker.adapter.AlbumAdapter;
import com.zzti.fengyongge.imagepicker.adapter.PhotoSelectorAdapter;
import com.zzti.fengyongge.imagepicker.control.PhotoSelectorDomain;
import com.zzti.fengyongge.imagepicker.dialog.AppSettingsDialog;
import com.zzti.fengyongge.imagepicker.model.AlbumModel;
import com.zzti.fengyongge.imagepicker.model.PhotoModel;
import com.zzti.fengyongge.imagepicker.util.CommonUtils;
import com.zzti.fengyongge.imagepicker.util.FileProviderUtil;
import com.zzti.fengyongge.imagepicker.util.FileUtils;
import com.zzti.fengyongge.imagepicker.view.SelectPhotoItem;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.bundle.IBundleManager;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.media.photokit.metadata.AVStorage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class PhotoSelectorAbility extends Ability implements SelectPhotoItem.ItemClickListener,
        SelectPhotoItem.PhotoItemCheckedListener, Component.ClickedListener {

    private static final int READ_MEDIA = 123;
    private static final int RC_CAMERA_PERM = 124;
    private static final int RC_SETTINGS_SCREEN = 125;
    private static final int CAMERA_REQUEST_CODE = 200;
    private static final int COLUMN_COUNT = 3;
    private Image ivBack;
    private Text tvPercent, tvTitle;
    public static final String RECCENT_PHOTO = "最近照片";
    private List<PhotoModel> singlePhotos = new ArrayList<PhotoModel>();
    private ListContainer gvPhotos;
    private PhotoSelectorAdapter photoAdapter;
    private ListContainer lvAblum;
    private AlbumAdapter albumAdapter;
    private Text tvAlbum, tvPreview;
    private PhotoSelectorDomain photoSelectorDomain;
    private DependentLayout layoutAlbum;
    public static ArrayList<PhotoModel> selected = new ArrayList<PhotoModel>();
    private List<String> imagePathList = new ArrayList<String>();
    private File takeImageFile;
    private boolean isShowCamera;
    private boolean isTakePhoto;
    private int limit;
    private EventHandler handler = new EventHandler(EventRunner.current()) {
        @Override
        public void processEvent(InnerEvent event) {
            successCallBack();
        }
    };

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_photo_selector);
        initView();
        initOnclick();
        writeTask();
    }

    void initView() {
        ivBack = (Image) findComponentById(ResourceTable.Id_ivBack);
        tvTitle = (Text) findComponentById(ResourceTable.Id_tvTitle);
        tvTitle.setText("最近照片");
        tvPercent = (Text) findComponentById(ResourceTable.Id_tvPercent);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(getContext().getColor(ResourceTable.Color_gray_44)));
        tvPercent.setBackground(element);
        gvPhotos = (ListContainer) findComponentById(ResourceTable.Id_gv_photos_ar);
        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(COLUMN_COUNT);
        gvPhotos.setLayoutManager(tableLayoutManager);
        lvAblum = (ListContainer) findComponentById(ResourceTable.Id_lv_ablum_ar);
        tvAlbum = (Text) findComponentById(ResourceTable.Id_tv_album_ar);
        tvPreview = (Text) findComponentById(ResourceTable.Id_tv_preview_ar);
        layoutAlbum = (DependentLayout) findComponentById(ResourceTable.Id_layout_album_ar);
        isShowCamera = getIntent().getBooleanParam(ImagePickerInstance.IS_SHOW_CAMERA, true);
        limit = getIntent().getIntParam(ImagePickerInstance.LIMIT, 0);
        photoSelectorDomain = new PhotoSelectorDomain(this);
        initPercent();
    }

    void initOnclick() {
        ivBack.setClickedListener(this);
        tvPercent.setClickedListener(this);
        tvAlbum.setClickedListener(this);
        tvPreview.setClickedListener(this);
    }

    void initPercent(){
        tvPercent.setText("完成");
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(getContext().getColor(ResourceTable.Color_gray_44)));
        tvPercent.setBackground(element);
        tvPercent.setTextColor(new Color(getContext().getColor(ResourceTable.Color_gray_69)));
    }

    public void showPic() {
        photoAdapter = new PhotoSelectorAdapter(this, new ArrayList<PhotoModel>(),
                CommonUtils.getWidthPixels(this), this, this, this, limit);
        gvPhotos.setItemProvider(photoAdapter);
        albumAdapter = new AlbumAdapter(this, new ArrayList<AlbumModel>());
        lvAblum.setItemProvider(albumAdapter);
        lvAblum.setItemClickedListener(new ListContainer.ItemClickedListener() {
            /** 相册列表点击事件 */
            @Override
            public void onItemClicked(ListContainer parent, Component component, int position, long id) {
                AlbumModel current = (AlbumModel) parent.getItemProvider().getItem(position);
                for (int i = 0; i < parent.getItemProvider().getCount(); i++) {
                    AlbumModel album = (AlbumModel) parent.getItemProvider().getItem(i);
                    if (i == position) {
                        album.setCheck(true);
                    } else {
                        album.setCheck(false);
                    }
                }
                albumAdapter.notifyDataChanged();
                hideAlbum();
                tvAlbum.setText(current.getName());
                tvTitle.setText(current.getName());

                // 更新照片列表
                if (current.getName().equals(RECCENT_PHOTO)) {
                    photoSelectorDomain.getReccent(recentListener);
                } else {
                    // 获取选中相册的照片
                    photoSelectorDomain.getAlbum(current.getName(), recentListener);
                }
            }
        });
        // 更新最近照片
        photoSelectorDomain.getReccent(recentListener);
        // 更新相册信息
        photoSelectorDomain.updateAlbum(albumListener);
    }

    /**
     * 拍照
     */
    private void takePhoto() {
        takeImageFile = FileUtils.getCreateFilePath(this);
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withAction("android.media.action.IMAGE_CAPTURE")
                .build();
        intent.setOperation(operation);
        // 下面这句指定调用相机拍照后的照片存储的路径
        intent.setParam(AVStorage.Images.Media.OUTPUT,
                FileProviderUtil.getFileUri(takeImageFile));
        startAbilityForResult(intent, CAMERA_REQUEST_CODE);
        isTakePhoto = true;
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == -1) {
                PhotoModel photoModel = new PhotoModel();
                try {
                    photoModel.setOriginalPath(takeImageFile.getCanonicalPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                selected.clear();
                selected.add(photoModel);
                ok();
            }
        }
    }

    /**
     * 完成
     */
    private void ok() {
        if (selected.isEmpty()) {
            setResult(0, null);
            terminateAbility();
        } else {
            imagePathList.clear();
            //拍照和图库选择分开处理
            if (isTakePhoto) {
                imagePathList.add(selected.get(0).getOriginalPath());
                FileUtils.updateGallery(PhotoSelectorAbility.this, takeImageFile);
                successCallBack();
            } else {
                new Thread() {
                    @Override
                    public void run() {
                        for (int i = 0; i < selected.size(); i++) {
                            imagePathList.add(selected.get(i).getOriginalPath());
                        }
                        handler.sendEvent(0);
                    }
                }.start();
            }
        }
    }

    /**
     * 将选择的数据返回给用户
     */
    private void successCallBack(){
        Intent data = new Intent();
        data.setStringArrayListParam("photos", (ArrayList<String>) imagePathList);
        setResult(-1, data);
        isTakePhoto = false;
        terminateAbility();
    }


    /**
     * 预览照片
     */
    private void priview() {
        Intent intent = new Intent();
        intent.setSequenceableArrayListParam("photos", new ArrayList<PhotoModel>(selected));
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(PhotoPreviewAbility.class)
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }

    private void album() {
        if (layoutAlbum.getVisibility() == Component.HIDE) {
            popAlbum();
        } else {
            hideAlbum();
        }
    }

    /**
     * 弹出相册列表
     */
    private void popAlbum() {
        layoutAlbum.setVisibility(Component.VISIBLE);
    }

    /**
     * 隐藏相册列表
     */
    private void hideAlbum() {
        layoutAlbum.setVisibility(Component.HIDE);
    }

    /**
     * 清空选中的图片
     */
    private void reset() {
        selected.clear();
        tvPreview.setText("预览");
        tvPreview.setEnabled(false);
        tvPreview.setTextColor(new Color(getContext().getColor(ResourceTable.Color_darker_gray)));
    }

    @Override
    /** 点击查看照片 */
    public void onItemClick(int position) {
        Intent intent = new Intent();
        if (isShowCamera) {
            if (tvAlbum.getText().equals(RECCENT_PHOTO)) {
                intent.setParam("position", position - 1);
            } else {
                intent.setParam("position", position);
            }
        } else {
            intent.setParam("position", position);
        }

        intent.setParam("album", tvAlbum.getText().toString());
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(PhotoPreviewAbility.class)
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }


    @Override
    /** 照片选中状态改变之后 */
    public void onCheckedChanged(PhotoModel photoModel, AbsButton buttonView, boolean isChecked) {
        if (isChecked) {
            selected.add(photoModel);
            tvPreview.setEnabled(true);
            tvPreview.setTextColor(new Color(getContext().getColor(ResourceTable.Color_white)));
        } else {
            selected.remove(photoModel);
        }
        tvPreview.setText("预览(" + selected.size() + ")");
        updatePercent(selected.size(), limit);
        if (selected.isEmpty()) {
            tvPreview.setEnabled(false);
            tvPreview.setText("预览");
            tvPreview.setTextColor(new Color(getContext().getColor(ResourceTable.Color_darker_gray)));
        }
    }

    /**
     * 展示当前张数
     */
    protected void updatePercent(int current, int totleNum) {
        if (current > 0) {
            tvPercent.setText("完成(" + current + "/" + totleNum + ")");
            ShapeElement element = new ShapeElement();
            element.setRgbColor(RgbColor.fromArgbInt(getContext().getColor(ResourceTable.Color_green_19)));
            tvPercent.setBackground(element);
            tvPercent.setTextColor(new Color(getContext().getColor(ResourceTable.Color_white)));
        } else {
            initPercent();
        }
    }

    @Override
    public void onClick(Component component) {
        int id = component.getId();
        if (id == ResourceTable.Id_ivBack) {
            terminateAbility();
        } else if (id == ResourceTable.Id_tvPercent) {
            ok();
        } else if (id == ResourceTable.Id_tv_album_ar) {
            album();
        } else if (id == ResourceTable.Id_tv_preview_ar) {
            priview();
        } else if (id == ResourceTable.Id_rlCamera) {
            cameraTask();
        }
    }

    private PhotoSelectorAbility.OnLocalAlbumListener albumListener = new PhotoSelectorAbility.OnLocalAlbumListener() {
        @Override
        public void onAlbumLoaded(List<AlbumModel> albums) {
            albumAdapter.update(albums);
        }
    };

    private PhotoSelectorAbility.OnLocalRecentListener recentListener = new PhotoSelectorAbility.OnLocalRecentListener() {
        @Override
        public void onPhotoLoaded(List<PhotoModel> photos) {
            if (isShowCamera) {
                photos.add(0, new PhotoModel());
            }
            singlePhotos.clear();
            singlePhotos.addAll(photos);
            photoAdapter.update(photos);
            gvPhotos.scrollTo(0); // 滚动到顶端
            reset();
        }
    };

    /**
     * 6.0动态申请获取相册权限
     */
    public void writeTask() {
        if (verifySelfPermission("ohos.permission.READ_MEDIA") != IBundleManager.PERMISSION_GRANTED) {
            // Ask for one permission
            if (canRequestPermission("ohos.permission.READ_MEDIA")) {
                String[] permission = {"ohos.permission.READ_MEDIA"};
                requestPermissionsFromUser(permission, READ_MEDIA);
            }
        } else {
            // Have permission, do the thing!
            showPic();
        }
    }

    /**
     * 6.0动态申请拍照权限
     */
    public void cameraTask() {
        if (verifySelfPermission("ohos.permission.CAMERA") != IBundleManager.PERMISSION_GRANTED) {
            // Ask for one permission
            if (canRequestPermission("ohos.permission.CAMERA")) {
                String[] permission = {"ohos.permission.CAMERA"};
                requestPermissionsFromUser(permission, RC_CAMERA_PERM);
            }
        } else {
            // Have permission, do the thing!
            takePhoto();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onBackPressed() {
        if (layoutAlbum.getVisibility() == Component.VISIBLE) {
            hideAlbum();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case READ_MEDIA:
                    showPic();
                    break;
                case RC_CAMERA_PERM:
                    takePhoto();
                    break;
            }
        } else {
            AppSettingsDialog dialog = new AppSettingsDialog(this);
            dialog.show();
        }
    }

    /**
     * 获取本地图库照片回调
     */
    public interface OnLocalRecentListener {
        void onPhotoLoaded(List<PhotoModel> photos);
    }

    /**
     * 获取本地相册信息回调
     */
    public interface OnLocalAlbumListener {
        void onAlbumLoaded(List<AlbumModel> albums);
    }

}
