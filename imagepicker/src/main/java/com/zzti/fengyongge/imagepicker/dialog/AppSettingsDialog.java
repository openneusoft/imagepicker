/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zzti.fengyongge.imagepicker.dialog;

import com.zzti.fengyongge.imagepicker.ResourceTable;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;

public class AppSettingsDialog extends CommonDialog {

    private  Component layout;
    private  Button btnCancel;
    private  Button btnSetting;

    public AppSettingsDialog(Context context) {
        super(context);
        layout = LayoutScatter.getInstance(context).parse(
                ResourceTable.Layout_layout_appsettings_dialog, null, true);
        setContentCustomComponent(layout);
        btnCancel = (Button) layout.findComponentById(ResourceTable.Id_btnCancel);
        btnSetting = (Button) layout.findComponentById(ResourceTable.Id_btnSetting);

        btnCancel.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                destroy();
                layout.getComponentParent().removeComponent(layout);
            }
        });
        btnSetting.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withUri(Uri.getUriFromParts("package", context.getBundleName(), null))
                        .withAction(IntentConstants.ACTION_APPLICATION_DETAILS_SETTINGS)
                        .withFlags(Intent.FLAG_ABILITY_NEW_MISSION)
                        .build();
                intent.setOperation(operation);
                context.startAbility(intent, 0);
                destroy();
                layout.getComponentParent().removeComponent(layout);
            }
        });
    }
}
