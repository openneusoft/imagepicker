package com.zzti.fengyongge.imagepicker.util;

import ohos.app.Context;
import ohos.media.image.ImagePacker;
import ohos.media.image.ImagePacker.PackingOptions;
import ohos.media.image.PixelMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 保存文件
 *
 * @author fengyongge
 * GitHub:https://github.com/fengyongge/imagepicker
 */
public final class FileUtilsExt {
    private static final int QUALITY = 100;
    private FileUtilsExt() {}
    /**
     * 创建需要保存的文件
     *
     * @param fileName              保存文件名
     * @param folderName            保存在sdcard根目录下的文件夹名
     */
    public static File savaFileUtils(
            Context context,
            String fileName,
            String folderName) {
        return new File(context.getExternalFilesDir(folderName), fileName);
    }

    /**
     * bitmap保存到File
     */
    public static void saveBitmap2File(Context context, PixelMap pixelMap, File file) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        PackingOptions packingOptions = new PackingOptions();
        packingOptions.format = "image/jpeg";
        packingOptions.quality = QUALITY;
        ImagePacker imagePacker = ImagePacker.create();
        imagePacker.initializePacking(fos, packingOptions);
        imagePacker.addImage(pixelMap);
        imagePacker.finalizePacking();
        try {
            fos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //通知系统图库更新
        NotifyUtils.refreshSystemPic(context, file);
    }
}