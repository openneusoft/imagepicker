/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zzti.fengyongge.imagepicker;

import com.zzti.fengyongge.imagepicker.ImageloderListener.ImageDownloadListener;
import com.zzti.fengyongge.imagepicker.ImageloderListener.Imageloder;
import com.zzti.fengyongge.imagepicker.dialog.AppSettingsDialog;
import com.zzti.fengyongge.imagepicker.model.PhotoModel;
import com.zzti.fengyongge.imagepicker.view.PhotoPreview;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;
import ohos.bundle.IBundleManager;

import java.util.ArrayList;
import java.util.List;

public class BasePhotoPreviewAbility extends Ability {

    protected List<PhotoModel> photos = new ArrayList<PhotoModel>();
    protected int current;
    private Boolean saveFlag;
    private PageSlider mViewPager;
    private DependentLayout rlTopTitle;
    private Component viewLine;
    private Image ivBack;
    private Text tvPercent;
    private String downloadUrl;
    private static final int WRITE_MEDIA = 123;
    private static final int DURATION = 2000;
    private  boolean isUp;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_photo_preview);
        initView();
        initOnclick();
    }

    final void initView() {
        viewLine = findComponentById(ResourceTable.Id_viewLine);
        viewLine.setVisibility(Component.HIDE);
        rlTopTitle = (DependentLayout) findComponentById(ResourceTable.Id_rlTopTitle);
        ivBack = (Image) findComponentById(ResourceTable.Id_ivBack);
        tvPercent = (Text) findComponentById(ResourceTable.Id_tvPercent);
        mViewPager = (PageSlider) findComponentById(ResourceTable.Id_viewPager);
        setTransitionAnimation(ResourceTable.Animation_ability_alpha_action_in, 0);
    }

    final void initOnclick() {
        if (ivBack != null) {
            ivBack.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    terminateAbility();
                }
            });
        }

        if (mViewPager != null) {
            mViewPager.addPageChangedListener(new PageSlider.PageChangedListener() {

                @Override
                public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageChosen(int position) {
                    current = position;
                    updatePercent(current,photos.size());
                }

                @Override
                public void onPageSlideStateChanged(int state) {

                }
            });
        }
    }

    /**
     * 绑定数据，更新界面
     */
    protected void bindData(Boolean saveFlag) {
        this.saveFlag = saveFlag;
        mViewPager.setProvider(mSliderProvider);
        mViewPager.setCurrentPage(current);
    }

    /**
     * 展示当前张数
     */
    protected void updatePercent(int current, int totalNum) {
        tvPercent.setText((current+1) + "/" + totalNum);
    }

    /**
     * 装载图片
     */
    private PageSliderProvider mSliderProvider = new PageSliderProvider() {
        @Override
        public int getCount() {
            if (photos == null) {
                return 0;
            } else {
                return photos.size();
            }
        }

        @Override
        public Object createPageInContainer(final ComponentContainer container, final int position) {
            PhotoPreview photoPreview = new PhotoPreview(BasePhotoPreviewAbility.this);
            photoPreview.loadImage(photos.get(position), saveFlag);
            photoPreview.setWidth(ComponentContainer.LayoutConfig.MATCH_PARENT);
            photoPreview.setHeight(ComponentContainer.LayoutConfig.MATCH_PARENT);
            if (photoPreview.saveBt != null) {
                photoPreview.saveBt.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        downloadUrl = photoPreview.downloadeUrl;
                        writeTask();
                    }
                });
            }
            container.addComponent(photoPreview);
            return photoPreview;
        }

        @Override
        public void destroyPageFromContainer(ComponentContainer container, int position, Object object) {
            container.removeComponent((Component) object);
        }

        @Override
        public boolean isPageMatchToObject(Component component, Object object) {
            return component == object;
        }
    };

    /**
     * 6.0动态申请获取相册权限
     */
    public void writeTask() {
        if (verifySelfPermission("ohos.permission.WRITE_MEDIA") != IBundleManager.PERMISSION_GRANTED) {
            // Ask for one permission
            if (canRequestPermission("ohos.permission.WRITE_MEDIA")) {
                String[] permission = {"ohos.permission.WRITE_MEDIA"};
                requestPermissionsFromUser(permission, WRITE_MEDIA);
            }
        } else {
            // Have permission, do the thing!
            download();
        }
    }

    private void download() {
        Imageloder.getInstance().asyncDownloadImage(this, downloadUrl,
                "IMG_" + System.currentTimeMillis() + ".jpg", "imagepicker",
                new ImageDownloadListener() {
            @Override
            public void onDownloadSuccess() {
                getUITaskDispatcher().asyncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        new ToastDialog(BasePhotoPreviewAbility.this)
                                .setText("保存成功")
                                .setDuration(DURATION)
                                .show();
                    }
                });
            }

            @Override
            public void onDownloadFail() {

            }
        });
    }

    @Override
    public final void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case WRITE_MEDIA:
                    download();
                    break;
            }
        } else {
            AppSettingsDialog dialog = new AppSettingsDialog(this);
            dialog.show();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
