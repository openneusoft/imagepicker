package com.zzti.fengyongge.imagepicker.adapter;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fengyongge on 2016/5/24
 */
public class MBaseAdapter<T> extends BaseItemProvider {
    protected  Context context;
    protected ArrayList<T> models;
    public MBaseAdapter(Context context, ArrayList<T> models) {
        this.context = context;
        if (models == null){
            this.models = new ArrayList<T>();
        }else{
            this.models = models;
        }
    }

    @Override
    public final int getCount() {
        if (models != null) {
            return models.size();
        }
        return 0;
    }

    @Override
    public final Object getItem(int position) {
        return models.get(position);
    }

    @Override
    public final long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer parent) {
        return null;
    }

    public final void update(List<T> models) {
        if (models == null){
            return;
        }
        this.models.clear();
        for (T t : models) {
            this.models.add(t);
        }
        notifyDataChanged();
    }

    public final ArrayList<T> getItems() {
        return models;
    }

}