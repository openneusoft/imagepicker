package com.zzti.fengyongge.imagepicker.control;

import com.zzti.fengyongge.imagepicker.model.AlbumModel;
import com.zzti.fengyongge.imagepicker.model.PhotoModel;
import com.zzti.fengyongge.imagepicker.util.LogUtils;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.app.Context;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.resultset.ResultSet;
import ohos.media.photokit.metadata.AVStorage.Images.Media;
import ohos.utils.net.Uri;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by fengyongge on 2016/5/24
 */
public class AlbumController {

    private DataAbilityHelper helper;
    private static final String BUCKET_DISPLAY_NAME = "bucket_display_name";
    private static final int MEDIA_SIZE = 1024;
    public AlbumController(Context context) {
        helper = DataAbilityHelper.creator(context);
    }

    /** 获取最近照片列表 */
    public List<PhotoModel> getCurrent() {
        List<PhotoModel> photos = new ArrayList<PhotoModel>();
        try {
            DataAbilityPredicates predicates = new DataAbilityPredicates();
            predicates.orderByAsc(Media.DATE_ADDED);
            ResultSet result = helper.query(Media.EXTERNAL_DATA_ABILITY_URI, new String[] {
                    Media.ID,
                    Media.DATE_ADDED,
                    Media.SIZE
            }, null);
            if (result == null || !result.goToNextRow()) {
                return photos;
            }
            result.goToLastRow();
            do {
                if (result.getLong(result.getColumnIndexForName(Media.SIZE)) > MEDIA_SIZE) {
                    PhotoModel photoModel = new PhotoModel();
                    int mediaId = result.getInt(result.getColumnIndexForName(Media.ID));
                    Uri uri = Uri.appendEncodedPathToUri(Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(mediaId));
                    photoModel.setOriginalPath(uri.toString());
                    photos.add(photoModel);
                }
            } while (result.goToPreviousRow());
        } catch (DataAbilityRemoteException e) {
            LogUtils.error(this.getClass().getSimpleName(), e.getMessage());
        }
        return photos;
    }

    /** 获取所有相册列表 */
    public List<AlbumModel> getAlbums() {
        List<AlbumModel> albums = new ArrayList<AlbumModel>();
        try {
            Map<String, AlbumModel> map = new HashMap<String, AlbumModel>();
            ResultSet result = helper.query(Media.EXTERNAL_DATA_ABILITY_URI, new String[]{
                    Media.ID,
                    BUCKET_DISPLAY_NAME,
                    Media.SIZE
            }, null);
            if (result == null || !result.goToNextRow()) {
                return albums;
            }
            result.goToLastRow();
            int mediaId = result.getInt(result.getColumnIndexForName(Media.ID));
            Uri uri = Uri.appendEncodedPathToUri(Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(mediaId));
            // "最近照片"相册
            AlbumModel current = new AlbumModel("最近照片", 0, uri.toString(), true);
            albums.add(current);
            do {
                if (result.getInt(result.getColumnIndexForName(Media.SIZE)) < MEDIA_SIZE) {
                    continue;
                }
                current.increaseCount();
                String name = result.getString(result.getColumnIndexForName(BUCKET_DISPLAY_NAME));
                if (map.keySet().contains(name)) {
                    map.get(name).increaseCount();
                } else {
                    mediaId = result.getInt(result.getColumnIndexForName(Media.ID));
                    uri = Uri.appendEncodedPathToUri(Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(mediaId));
                    AlbumModel album = new AlbumModel(name, 1, uri.toString());
                    map.put(name, album);
                    albums.add(album);
                }
            } while (result.goToPreviousRow());
        } catch (DataAbilityRemoteException e) {
            LogUtils.error(this.getClass().getSimpleName(), e.getMessage());
        }
        return albums;
    }

    /** 获取对应相册下的照片 */
    public List<PhotoModel> getAlbum(String name) {
        List<PhotoModel> photos = new ArrayList<PhotoModel>();
        try {
            DataAbilityPredicates predicates = new DataAbilityPredicates();
            predicates.equalTo(BUCKET_DISPLAY_NAME, name);
            predicates.orderByAsc(Media.DATE_ADDED);
            ResultSet result = helper.query(Media.EXTERNAL_DATA_ABILITY_URI, new String[] {
                    BUCKET_DISPLAY_NAME,
                    Media.ID,
                    Media.DATE_ADDED,
                    Media.SIZE
            }, predicates);
            if (result == null || !result.goToNextRow()) {
                return photos;
            }
            result.goToLastRow();
            do {
                if (result.getLong(result.getColumnIndexForName(Media.SIZE)) > MEDIA_SIZE) {
                    int mediaId = result.getInt(result.getColumnIndexForName(Media.ID));
                    Uri uri = Uri.appendEncodedPathToUri(Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(mediaId));
                    PhotoModel photoModel = new PhotoModel();
                    photoModel.setOriginalPath(uri.toString());
                    photos.add(photoModel);
                }
            } while (result.goToPreviousRow());
        } catch (DataAbilityRemoteException e) {
            LogUtils.error(this.getClass().getSimpleName(), e.getMessage());
        }
        return photos;
    }
}
