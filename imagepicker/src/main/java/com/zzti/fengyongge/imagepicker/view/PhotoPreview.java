package com.zzti.fengyongge.imagepicker.view;

import com.zzti.fengyongge.imagepicker.ImageloderListener.Imageloder;
import com.zzti.fengyongge.imagepicker.ResourceTable;
import com.zzti.fengyongge.imagepicker.model.PhotoModel;
import ohos.agp.components.*;
import ohos.app.Context;

public final class PhotoPreview extends DirectionalLayout implements Component.ClickedListener {
    private ProgressBar pbLoading;
    private Image ivContent;
    private ClickedListener l;
    public Text saveBt;
    public String downloadeUrl;
    private Context cxt;

    public PhotoPreview(Context context) {
        super(context);
        this.cxt = context;
        LayoutScatter.getInstance(context).parse(
                ResourceTable.Layout_view_photopreview, this, true);

        pbLoading = (ProgressBar) findComponentById(ResourceTable.Id_pb_loading_vpp);
        ivContent = (Image) findComponentById(ResourceTable.Id_iv_content_vpp);
        saveBt = (Text) findComponentById(ResourceTable.Id_save_bt);
        ivContent.setClickedListener(this);
    }

    @Override
    public void setClickedListener(ClickedListener l) {
        this.l = l;
    }

    @Override
    public void onClick(Component component) {
        if (component.getId() == ResourceTable.Id_iv_content_vpp && l != null) {
            l.onClick(ivContent);
        }
    }

    public PhotoPreview(Context context, AttrSet attrs, int defStyle) {
        this(context);
    }

    public PhotoPreview(Context context, AttrSet attrs) {
        this(context);
    }

    public void loadImage(PhotoModel photoModel, boolean saveFlag) {
        if (saveFlag) {
            saveBt.setVisibility(Component.VISIBLE);
        } else {
            saveBt.setVisibility(Component.HIDE);
        }

        //根据是否是网络图片，调用不同展示
        if (photoModel.getOriginalPath().startsWith("http") || photoModel.getOriginalPath().startsWith("https")) {
            downloadeUrl = photoModel.getOriginalPath();
        }
        Imageloder.getInstance().loadImageUrl(cxt, photoModel.getOriginalPath(), ivContent);
    }
}
