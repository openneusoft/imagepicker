package com.zzti.fengyongge.imagepicker.view;

import com.zzti.fengyongge.imagepicker.ImageloderListener.Imageloder;
import com.zzti.fengyongge.imagepicker.PhotoSelectorAbility;
import com.zzti.fengyongge.imagepicker.ResourceTable;
import com.zzti.fengyongge.imagepicker.model.PhotoModel;
import com.zzti.fengyongge.imagepicker.util.LogUtils;
import ohos.agp.components.*;
import ohos.agp.components.AbsButton.CheckedStateChangedListener;
import ohos.agp.components.Component.ClickedListener;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.global.resource.NotExistException;

import java.io.IOException;

/**
 * Created by fengyongge on 2016/5/24
 */
public final class SelectPhotoItem extends DirectionalLayout implements CheckedStateChangedListener, ClickedListener {
    private Image ivPhoto;
    private Checkbox cbPhoto;
    private Component cbPhotoRl;
    private PhotoItemCheckedListener listener;
    private PhotoModel photo;
    private boolean isCheckAll;
    private ItemClickListener l;
    private int position;
    private int limit;
    private Context context;
    private static final int DURATION = 1000;
    private SelectPhotoItem(Context context) {
        super(context);
    }

    public SelectPhotoItem(final Context context, final PhotoItemCheckedListener listener,final int limit) {
        this(context);
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_photoitem, this, true);
        this.listener = listener;
        this.limit = limit;
        this.context = context;

        setClickedListener(this);
        ivPhoto = (Image) findComponentById(ResourceTable.Id_iv_photo_lpsi);
        cbPhoto = (Checkbox) findComponentById(ResourceTable.Id_cb_photo_lpsi);
        cbPhotoRl = findComponentById(ResourceTable.Id_cb_photo_RL);

        try {
            PixelMapElement checkOn =
                    new PixelMapElement(context.getResourceManager().getResource(ResourceTable.Media_ic_check_pic));
            PixelMapElement checkOff =
                    new PixelMapElement(context.getResourceManager().getResource(ResourceTable.Media_ic_uncheck_pic));

            StateElement checkElement = new StateElement();
            checkElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, checkOn);
            checkElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, checkOff);
            cbPhoto.setButtonElement(checkElement);
        } catch(IOException e) {
            LogUtils.error(context.getClass().getSimpleName(), e.getMessage());
        } catch(NotExistException e) {
            LogUtils.error(context.getClass().getSimpleName(), e.getMessage());
        }

        cbPhotoRl.setClickedListener(new ClickedListener() {

            @Override
            public void onClick(Component arg0) {
                if (!cbPhoto.isChecked()) {
                    if (PhotoSelectorAbility.selected.size() >= limit) {
                        new ToastDialog(context).setText("最多添加"  + limit +"张").setDuration(DURATION).show();
                        return;
                    }
                }
                cbPhoto.setChecked(!cbPhoto.isChecked());
                boolean isChecked = cbPhoto.isChecked();
                if (!isCheckAll) {
                    listener.onCheckedChanged(photo, null, isChecked);
                }

                photo.setChecked(isChecked);
            }
        });
    }

    @Override
    public void onCheckedChanged(AbsButton buttonView, boolean isChecked) {
        if (!isCheckAll) {
            listener.onCheckedChanged(photo, buttonView, isChecked);
        }
        photo.setChecked(isChecked);
    }

    public void setImageDrawable(final PhotoModel photo) {
        this.photo = photo;
        new EventHandler(EventRunner.current()).postSyncTask(new Runnable() {
            @Override
            public void run() {
                Imageloder.getInstance().loadImageUrl(context, photo.getOriginalPath(), ivPhoto);
            }
        });
    }

    @Override
    public void setSelected(boolean selected) {
        if (photo == null) {
            return;
        }
        isCheckAll = true;
        cbPhoto.setChecked(selected);
        isCheckAll = false;
    }

    public void setClickedListener(ItemClickListener l, int position) {
        this.l = l;
        this.position = position;
    }

    @Override
    public void onClick(Component v) {
        if (l != null){
            l.onItemClick(position);
        }
    }

    public interface PhotoItemCheckedListener {
        void onCheckedChanged(PhotoModel photoModel, AbsButton buttonView, boolean isChecked);
    }

    public interface ItemClickListener {
        void onItemClick(int position);
    }
}
