# imagepicker(图片选择器)

**本项目是基于开源项目imagepicker进行ohos化的移植和开发的，可以通过项目标签以及github地址
（https://gitee.com/openneusoft/imagepicker ）追踪到原项目版本**

#### 项目介绍

- 项目名称：图片选择器
- 所属系列：ohos的第三方组件适配移植
- 功能：imagepicker是一款用于在设备上获取照片（拍照或从相册、文件中选择）、压缩图片的开源工具库。
　	   从相册里面选择图片或者拍照获取照片
　      浏览选择的本地或者网络图片
　      保存图片
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：fengyongge
- 联系方式：fengyongge98@gmail.com 
- 原项目Doc地址：https://github.com/fengyongge/imagepicker
- 原项目基线版本：v1.4.5 
- 编程语言：Java
- 外部库依赖：无

#### 效果展示

<img src="image0/image.gif"/>

#### 安装教程

方法1.

1. 编译har包imagepicker.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
dependencies {
	implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。
  
方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
	maven {
		url 'https://s01.oss.sonatype.org/content/repositories/releases/'
	}
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
	dependencies {
		implementation 'io.github.dzsf:imagepicker:1.0.0'
	}
```

#### 使用说明

1. 拍照或者从图库选择图片

```
    //获取单例，调用下面方法即可，具体可参考源码sample
    ImagePickerInstance.getInstance()

    /**
    * 对外图库选择图片,或者拍照选择图片方法
    * @param abilitySlice
    * @param limit  选择图片张数
    * @param isShowCamera 是否支持拍照
    * @param requestCode
    */
    public void photoSelect(AbilitySlice abilitySlice, int limit, boolean isShowCamera, int requestCode) {
        Intent intent = new Intent();
        intent.setParam(LIMIT, limit);
        intent.setParam(IS_SHOW_CAMERA, false);
        Operation operation = new Intent.OperationBuilder()
              .withDeviceId("")
              .withBundleName(abilitySlice.getBundleName())
              .withAbilityName(PhotoSelectorAbility.class)
              .build();
        intent.setOperation(operation);
        abilitySlice.startAbilityForResult(intent, requestCode);
    }
```

2. 获取拍照或者图片地址

```
  	@Override
  	protected void onAbilityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == -1) {
                PhotoModel photoModel = new PhotoModel();
                try {
                    photoModel.setOriginalPath(takeImageFile.getCanonicalPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                selected.clear();
                selected.add(photoModel);
                ok();
            }
        }
    }
```

3. 浏览图片

```
    //获取单例，调用下面方法即可，具体可参考源码sample
    ImagePickerInstance.getInstance()

    /**
     * 对外开放的图片预览方法
     * @param context
     * @param tempList 浏览图片集合，注意！必须封装成imagepicker的bean，url支持网络或者本地
     * @param position  角标
     * @param isSave 是否支持保存
     */
    public void photoPreview(Context context, ArrayList<PhotoModel> tempList, int position, boolean isSave) {
        Intent intent = new Intent();
        intent.setSequenceableArrayListParam(PHOTOS, tempList);
        intent.setParam(POSITION, position);
        intent.setParam(IS_SAVE, isSave);
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(context.getBundleName())
                .withAbilityName(PhotoPreviewAbility.class)
                .build();
        intent.setOperation(operation);
        context.startAbility(intent, 0);
    }
```

4. 保存图片
	网络图片点击保存时，默认保存到内存卡，imagepicker文件夹下


#### 版本迭代

- v1.0.1

#### 版权和许可信息
- Apache Licence

#### LICENSE
Copyright 2016 fengyongge

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.